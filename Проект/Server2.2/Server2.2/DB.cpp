#include "pch.h"
#include "DB.h"
DB::DB() {

}

int DB::Select(std::string login, std::string password, int* type) {
    std::string query = "SELECT id, login, password, type FROM login_person where login = '" + login + "'";
    String^ queryDebil = msclr::interop::marshal_as<String^>(query);
    SqlCommand^ cmd = gcnew SqlCommand(queryDebil, conn);
    conn->Open();
    SqlDataReader^ reader = cmd->ExecuteReader();
    if (reader->Read()) {
        if (msclr::interop::marshal_as<String^>(password) == reader["password"]->ToString()) {
            int res = Convert::ToInt32(reader["id"]->ToString());
            *type = Convert::ToInt32(reader["type"]->ToString());
            conn->Close();
            return res;
        }
        else {
            conn->Close();
            return -2;
        }
    }
    else {
        conn->Close();
        return -1;
    }
}
void DB::ConnectToDB() {
    connStringBuilder = gcnew SqlConnectionStringBuilder();
    connStringBuilder->DataSource = "DESKTOP-51KRCB4\\SQLEXPRESS";
    connStringBuilder->InitialCatalog = "Queue";
    connStringBuilder->IntegratedSecurity = true;
    conn = gcnew SqlConnection(Convert::ToString(connStringBuilder));
}
bool DB::ExistOfLogin(std::string login) {
  
    bool isExist = false;
    std::string cmdText = "select login from login_person where login like '"+login+"' ";
    SqlCommand^ cmd = gcnew SqlCommand(msclr::interop::marshal_as<String^>(cmdText), conn);
    conn->Open();
    SqlDataReader^ reader = cmd->ExecuteReader();
    if (reader->Read())
    {
        isExist = true;
    }
    conn->Close();
    return isExist;
}

void DB::Insert(std::string name, std::string lastName, std::string login, std::string password) {
    ConnectToDB();
    std::string cmdText = "insert into login_person(name,lastName,login,password,type)values('" ;
    cmdText += name + "', '" + lastName + "', '" + login + "', '" + password + "', " + "'1') ";
    cmdText += "insert into queue values ('" + login + "', 0,0,0) ";
    cmdText += "update queue set firstQueue = (select max(firstQueue) as max_first from queue) + 1 where login like '"+login+"' ";
    cmdText += "update queue set secondQueue = (select max(secondQueue) as max_second from queue) + 1 where login like '" + login + "' ";
    cmdText += "update queue set thirdQueue =(select max(thirdQueue) as max_third from queue)+ 1 where login like '" + login + "' ";
    SqlCommand^ cmd = gcnew SqlCommand( msclr::interop::marshal_as<String^>(cmdText), conn);
    conn->Open();
    cmd->ExecuteNonQuery();
    conn->Close();
}

std::string DB::SendNumInQueue1(SOCKET con, int id){
    String^ query = "select firstQueue from [queue] where id = "+ id;
    SqlCommand^ cmd = gcnew SqlCommand(query, conn);
    conn->Open();
    SqlDataReader^ reader = cmd->ExecuteReader();
    reader->Read();
    nlohmann::json info{};
    info["Queue1"] = msclr::interop::marshal_as<std::string>(reader["firstQueue"]->ToString());//<--
    std::string str = info.dump();
    int sizeOfString = str.size();
    send(con, (char*)&sizeOfString, sizeof(int), NULL);
    send(con, str.c_str(), sizeOfString, NULL);
    conn->Close();
    return str;
}
std::string DB::SendNumInQueue2(SOCKET con, int id) {
    String^ query = "select secondQueue from [queue] where id = " + id;
    SqlCommand^ cmd = gcnew SqlCommand(query, conn);
    conn->Open();
    SqlDataReader^ reader = cmd->ExecuteReader();
    reader->Read();
    nlohmann::json info{};
    info["Queue2"] = msclr::interop::marshal_as<std::string>(reader["secondQueue"]->ToString());//<--
    std::string str = info.dump();
    int sizeOfString = str.size();
    send(con, (char*)&sizeOfString, sizeof(int), NULL);
    send(con, str.c_str(), sizeOfString, NULL);
    conn->Close();
    return str;
}
std::string DB::SendNumInQueue3(SOCKET con, int id) {
    String^ query = "select thirdQueue from [queue] where id = " + id;
    SqlCommand^ cmd = gcnew SqlCommand(query, conn);
    conn->Open();
    SqlDataReader^ reader = cmd->ExecuteReader();
    reader->Read();
    nlohmann::json info{};
    info["Queue3"] = msclr::interop::marshal_as<std::string>(reader["thirdQueue"]->ToString());//<--
    std::string str = info.dump();
    int sizeOfString = str.size();
    send(con, (char*)&sizeOfString, sizeof(int), NULL);
    send(con, str.c_str(), sizeOfString, NULL);
    conn->Close();
    return str;
}

void DB::SendQueue(SOCKET con/*, int queue*/) {
    String^ query = "select name, lastName,firstQueue,secondQueue,thirdQueue from "
        "login_person "
        "inner join queue "
       "on login_person.login = queue.login";
    SqlCommand^ cmd = gcnew SqlCommand(query, conn);
    conn->Open();
    SqlDataReader^ reader = cmd->ExecuteReader();
    nlohmann::json table{};
    bool isRead;
    while (reader->Read()) {
        isRead = true;
        send(con, (char*)&isRead, sizeof(bool), NULL);
        table["name"] = msclr::interop::marshal_as<std::string>(reader["name"]->ToString());
        table["lastName"] = msclr::interop::marshal_as<std::string>(reader["lastName"]->ToString());
        table["firstQueue"] = msclr::interop::marshal_as<std::string>(reader["firstQueue"]->ToString());
        table["secondQueue"] = msclr::interop::marshal_as<std::string>(reader["secondQueue"]->ToString());
        table["thirdQueue"] = msclr::interop::marshal_as<std::string>(reader["thirdQueue"]->ToString());
        std::string str = table.dump();

        int sizeOfString = str.size();
        send(con, (char*)&sizeOfString, sizeof(int), NULL);
        send(con, str.c_str(), sizeOfString, NULL);
    }
    conn->Close();
    isRead = false;
    send(con, (char*)&isRead, sizeof(bool), NULL);
}
void DB::ChangeQ1(SOCKET con) {
    String^ query = "update queue "
        "set firstQueue = firstQueue - 1 where firstQueue != 0";
    SqlCommand^ cmd = gcnew SqlCommand(query, conn);
    conn->Open();
    SqlDataReader^ reader = cmd->ExecuteReader();
    conn->Close();
}
void DB::ChangeQ2(SOCKET con) {
    String^ query = "update queue "
        "set secondQueue = secondQueue - 1 where secondQueue != 0";
    SqlCommand^ cmd = gcnew SqlCommand(query, conn);
    conn->Open();
    SqlDataReader^ reader = cmd->ExecuteReader();
    conn->Close();
}
void DB::ChangeQ3(SOCKET con) {
    String^ query = "update queue "
        "set thirdQueue = thirdQueue - 1 where thirdQueue != 0";
    SqlCommand^ cmd = gcnew SqlCommand(query, conn);
    conn->Open();
    SqlDataReader^ reader = cmd->ExecuteReader();
    conn->Close();
}