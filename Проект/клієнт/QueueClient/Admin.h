#pragma once
#include <msclr\marshal_cppstd.h>
#include <vector>
#include <nlohmann/json.hpp>
#include "Requests.h"
namespace QueueClient {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Admin
	/// </summary>
	public ref class Admin : public System::Windows::Forms::Form
	{
	public:
		Admin(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Admin()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ button1;
	protected:
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::DataGridView^ dataGridView1;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column5;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column3;









	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(Admin::typeid));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->Column4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->button4 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(24, 99);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(128, 41);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Glychov queue";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Admin::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(24, 171);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(128, 40);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Shologon queue";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Admin::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(24, 235);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(128, 38);
			this->button3->TabIndex = 2;
			this->button3->Text = L"Tsygylyk queue";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Admin::button3_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->BackColor = System::Drawing::Color::Transparent;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.25F));
			this->label1->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label1->Location = System::Drawing::Point(21, 63);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(101, 17);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Change queue";
			// 
			// dataGridView1
			// 
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
				this->Column4,
					this->Column5, this->Column1, this->Column2, this->Column3
			});
			this->dataGridView1->Location = System::Drawing::Point(185, 80);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->Size = System::Drawing::Size(394, 210);
			this->dataGridView1->TabIndex = 4;
			// 
			// Column4
			// 
			this->Column4->HeaderText = L"Name";
			this->Column4->Name = L"Column4";
			// 
			// Column5
			// 
			this->Column5->HeaderText = L"Last Name";
			this->Column5->Name = L"Column5";
			// 
			// Column1
			// 
			this->Column1->HeaderText = L"First Queue";
			this->Column1->MaxInputLength = 325;
			this->Column1->Name = L"Column1";
			this->Column1->Width = 50;
			// 
			// Column2
			// 
			this->Column2->HeaderText = L"Second Queue";
			this->Column2->Name = L"Column2";
			this->Column2->Width = 50;
			// 
			// Column3
			// 
			this->Column3->HeaderText = L"Third Queue";
			this->Column3->Name = L"Column3";
			this->Column3->Width = 50;
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(511, 353);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(87, 35);
			this->button4->TabIndex = 5;
			this->button4->Text = L"Exit";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Admin::button4_Click);
			// 
			// Admin
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"$this.BackgroundImage")));
			this->ClientSize = System::Drawing::Size(663, 421);
			this->Controls->Add(this->dataGridView1);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Name = L"Admin";
			this->Text = L"Admin";
			this->Load += gcnew System::EventHandler(this, &Admin::Admin_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private:
		SOCKET Connection;
		int ID;
	public:
		Admin(SOCKET conn, int ID) {
			InitializeComponent();
			Connection = conn;
			this->ID = ID;
		}
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
		dataGridView1->Rows->Clear();
		dataGridView1->Refresh();
		Request change2 = R_Change2;
		send(Connection, (char*)&change2, sizeof(int), NULL);
		LoadTable();
	}
	void LoadTable() {
		try {
			Request news = R_ShowQueue;
			send(Connection, (char*)&news, sizeof(int), NULL);
			bool state = true;
			recv(Connection, (char*)&state, sizeof(bool), NULL);
			while (state) {
				int msg_size;
				recv(Connection, (char*)&msg_size, sizeof(int), NULL);
				char* recvdata = new char[msg_size + 1];
				recvdata[msg_size] = '\0';
				recv(Connection, recvdata, msg_size, NULL);
				std::string str = recvdata;
				nlohmann::json str1 = nlohmann::json::parse(str);
				dataGridView1->Rows->Add(
					msclr::interop::marshal_as<String^>(str1["name"].get<std::string>()),
					msclr::interop::marshal_as<String^>(str1["lastName"].get<std::string>()),
					msclr::interop::marshal_as<String^>(str1["firstQueue"].get<std::string>()),
					msclr::interop::marshal_as<String^>(str1["secondQueue"].get<std::string>()),
					msclr::interop::marshal_as<String^>(str1["thirdQueue"].get<std::string>()));
				recv(Connection, (char*)&state, sizeof(bool), NULL);
			}
		}
		catch (...) {
			MessageBox::Show("Something went wrong.\nMaybe, try later", "Info", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
		}
		   }
	private: System::Void Admin_Load(System::Object^ sender, System::EventArgs^ e) {
		LoadTable();
	}
private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) {
	this->Close();
}
private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
	dataGridView1->Rows->Clear();
	dataGridView1->Refresh();
	Request change1 = R_Change1;
	send(Connection, (char*)&change1, sizeof(int), NULL);
	LoadTable();
}
private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
	dataGridView1->Rows->Clear();
	dataGridView1->Refresh();
	Request change3 = R_Change3;
	send(Connection, (char*)&change3, sizeof(int), NULL);
	LoadTable();
}
};
}
