#pragma once
#include <winsock2.h>
#include <iostream>
#include <msclr\marshal_cppstd.h>
#include <vector>
#include "Requests.h"
#include "Customer.h"
#include "Admin.h"
#include "Registration.h"

#pragma comment(lib, "ws2_32.lib")
#pragma warning(disable: 4996)
#define _WINSOCK_DEPRECATED_NO_WARNINGS
namespace QueueClient {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Data::SqlClient;
	using namespace System::Text;

	/// <summary>
	/// Summary for Login
	/// </summary>
	public ref class Login : public System::Windows::Forms::Form
	{
	public:
		Login(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Login()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^ textBox1;
	protected:
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(Login::typeid));
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(193, 129);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(177, 20);
			this->textBox1->TabIndex = 0;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(228, 226);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(99, 36);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Log In";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Login::button1_Click);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(193, 181);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(177, 20);
			this->textBox2->TabIndex = 2;
			this->textBox2->UseSystemPasswordChar = true;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->BackColor = System::Drawing::Color::Transparent;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.25F));
			this->label1->Location = System::Drawing::Point(190, 109);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(43, 17);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Login";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(714, 655);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(35, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"label2";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->BackColor = System::Drawing::Color::Transparent;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.25F));
			this->label3->Location = System::Drawing::Point(190, 161);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(69, 17);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Password";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(381, 316);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(88, 39);
			this->button2->TabIndex = 6;
			this->button2->Text = L"Sign In";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Login::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(487, 316);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(85, 39);
			this->button3->TabIndex = 7;
			this->button3->Text = L"Exit";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Login::button3_Click);
			// 
			// Login
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"$this.BackgroundImage")));
			this->ClientSize = System::Drawing::Size(600, 385);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox1);
			this->ForeColor = System::Drawing::Color::Coral;
			this->Name = L"Login";
			this->Text = L"Login";
			this->Load += gcnew System::EventHandler(this, &Login::Login_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private:
		SOCKET Connection;

	private: System::Void Login_Load(System::Object^ sender, System::EventArgs^ e) {
		WSAData wsaData;
		WORD DLLVersion = MAKEWORD(2, 1);
		if (WSAStartup(DLLVersion, &wsaData) != 0) {
			MessageBox::Show("Not connected", "Info", MessageBoxButtons::OK, MessageBoxIcon::Error);
			exit(1);
		}

		SOCKADDR_IN addr;
		int sizeofaddr = sizeof(addr);
		addr.sin_addr.s_addr = inet_addr("127.0.0.1");
		//addr.sin_addr.s_addr = inet_addr("95.134.123.45");
		addr.sin_port = htons(5000);
		addr.sin_family = AF_INET;

		Connection = socket(AF_INET, SOCK_STREAM, NULL);
		if (connect(Connection, (SOCKADDR*)&addr, sizeof(addr)) != 0) {
			MessageBox::Show("Not connected", "Info", MessageBoxButtons::OK, MessageBoxIcon::Error);
			return ;
		}
		else
			MessageBox::Show("Connected", "Info", MessageBoxButtons::OK, MessageBoxIcon::Information);
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
		try {
			Request login = R_Login;
			String^ log = textBox1->Text;
			std::string log1 = msclr::interop::marshal_as<std::string>(log);
			String^ password = textBox2->Text;
			std::string password1 = msclr::interop::marshal_as<std::string>(password);
			int sizeOfString = log1.size();
			if (log1 == "") {
				MessageBox::Show("Login fild is empty", "Info", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
				
			}else{
				if (password1 == "") {
					MessageBox::Show("Password fild is empty", "Info", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
				}
				else {
					send(Connection, (char*)&login, sizeof(int), NULL);
					send(Connection, (char*)&sizeOfString, sizeof(int), NULL);
					send(Connection, log1.c_str(), sizeOfString, NULL);
					sizeOfString = password1.size();
					send(Connection, (char*)&sizeOfString, sizeof(int), NULL);
					send(Connection, password1.c_str(), sizeOfString, NULL);
					int resReq;
					int type;

					recv(Connection, (char*)&resReq, sizeof(int), NULL);
					recv(Connection, (char*)&type, sizeof(int), NULL);
					if (resReq == -1 ) {
						MessageBox::Show("The login isn't correct!", "Info", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
					}
					else if (resReq == -2 || textBox2->Text == "") {
						MessageBox::Show("The password isn't correct!", "Info", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
					}
					else if (type == 2) {
						this->Hide();
						Admin^ admin = gcnew Admin(Connection, resReq);
						admin->ShowDialog();
						this->Show();
					}
					else if (type == 1) {
						this->Hide();
						Customer^ custumer = gcnew Customer(Connection, resReq);
						custumer->ShowDialog();
						this->Show();
					}
				}
			}
			
		}
		catch (...) {
			MessageBox::Show("Something went wrong.\nMaybe, try later", "Info", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
		}
	}
private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
	this->Hide();
	Registration^ registration = gcnew Registration(Connection);
	registration->ShowDialog();
	this->Show();
}
private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
	this->Close();
}
};
}
