#pragma once
#include <msclr\marshal_cppstd.h>
#include <vector>
#include <nlohmann/json.hpp>
#include "Requests.h"
namespace QueueClient {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Customer
	/// </summary>
	public ref class Customer : public System::Windows::Forms::Form
	{
	public:
		Customer(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Customer()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ button1;
	protected:
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::TextBox^ textBox3;

	private: System::Windows::Forms::Button^ button5;




	protected:



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(Customer::typeid));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(101, 107);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(133, 48);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Gas queue";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Customer::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(101, 188);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(133, 49);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Water queue";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Customer::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(101, 270);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(133, 48);
			this->button3->TabIndex = 2;
			this->button3->Text = L"Electricity queue";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Customer::button3_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->BackColor = System::Drawing::Color::Transparent;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12.25F));
			this->label1->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label1->Location = System::Drawing::Point(141, 61);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(65, 20);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Sign up";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->BackColor = System::Drawing::Color::Transparent;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12.25F));
			this->label2->ForeColor = System::Drawing::SystemColors::ControlLight;
			this->label2->Location = System::Drawing::Point(263, 61);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(164, 20);
			this->label2->TabIndex = 4;
			this->label2->Text = L"Number in the queue";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(298, 122);
			this->textBox1->Name = L"textBox1";
			this->textBox1->ReadOnly = true;
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 5;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(298, 203);
			this->textBox2->Name = L"textBox2";
			this->textBox2->ReadOnly = true;
			this->textBox2->Size = System::Drawing::Size(100, 20);
			this->textBox2->TabIndex = 6;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(298, 285);
			this->textBox3->Name = L"textBox3";
			this->textBox3->ReadOnly = true;
			this->textBox3->Size = System::Drawing::Size(100, 20);
			this->textBox3->TabIndex = 7;
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(473, 356);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(92, 43);
			this->button5->TabIndex = 9;
			this->button5->Text = L"Exit";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Customer::button5_Click);
			// 
			// Customer
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"$this.BackgroundImage")));
			this->ClientSize = System::Drawing::Size(610, 433);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Name = L"Customer";
			this->Text = L"Customer";
			this->Load += gcnew System::EventHandler(this, &Customer::Customer_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private:
		SOCKET Connection;
		int ID;
	public:
		Customer(SOCKET conn, int ID) {
		InitializeComponent();
		Connection = conn;
		this->ID = ID;
		}
	private: System::Void label2_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void radioButton3_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	}
private: System::Void Customer_Load(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
	Request req = R_SignInOnQueue1;
	send(Connection, (char*)&req, sizeof(int), NULL);
	int id = this->ID;
	send(Connection, (char*)&id, sizeof(int), NULL);
	int msg_size;
	recv(Connection, (char*)&msg_size, sizeof(int), NULL);
	char* recvdata = new char[msg_size + 1];
	recvdata[msg_size] = '\0';
	recv(Connection, recvdata, msg_size, NULL);
	std::string str = recvdata;
	nlohmann::json str1 = nlohmann::json::parse(str);
	textBox1->Text = msclr::interop::marshal_as<String^>(str1["Queue1"].get<std::string>());
}
private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
	Request req = R_SignInOnQueue2;
	send(Connection, (char*)&req, sizeof(int), NULL);
	int id = this->ID;
	send(Connection, (char*)&id, sizeof(int), NULL);
	int msg_size;
	recv(Connection, (char*)&msg_size, sizeof(int), NULL);
	char* recvdata = new char[msg_size + 1];
	recvdata[msg_size] = '\0';
	recv(Connection, recvdata, msg_size, NULL);
	std::string str = recvdata;
	nlohmann::json str1 = nlohmann::json::parse(str);
	textBox2->Text = msclr::interop::marshal_as<String^>(str1["Queue2"].get<std::string>());
}
private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
	Request req = R_SignInOnQueue3;
	send(Connection, (char*)&req, sizeof(int), NULL);
	int id = this->ID;
	send(Connection, (char*)&id, sizeof(int), NULL);
	int msg_size;
	recv(Connection, (char*)&msg_size, sizeof(int), NULL);
	char* recvdata = new char[msg_size + 1];
	recvdata[msg_size] = '\0';
	recv(Connection, recvdata, msg_size, NULL);
	std::string str = recvdata;
	nlohmann::json str1 = nlohmann::json::parse(str);
	textBox3->Text = msclr::interop::marshal_as<String^>(str1["Queue3"].get<std::string>());
}

private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e) {
	this->Close();
}
};
}
